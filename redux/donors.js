import * as ActionTypes from './ActionTypes';

export const donors = (state = { isLoading: true,
                                 errMess: null,
                                 donors:[]}, action) => {
    switch (action.type) {
        case ActionTypes.ADD_DONORS:
            return {...state, isLoading: false, errMess: null, donors: action.payload};

        case ActionTypes.DONORS_LOADING:
            return {...state, isLoading: true, errMess: null, donors: []}

        case ActionTypes.DONORS_FAILED:
            return {...state, isLoading: false, errMess: action.payload};

        default:
          return state;
      }
};