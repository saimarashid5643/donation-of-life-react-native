import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { donors } from './donors';
import { persistStore, persistCombineReducers } from 'redux-persist';
import storage from 'redux-persist/es/storage';
import { AsyncStorage } from 'react-native';



export const ConfigureStore = () => {
    const config = {
        key: 'root',
        storage: AsyncStorage,
        debug: true
      }
    const store = createStore(
        persistCombineReducers(config, {
            donors
        }),
        applyMiddleware(thunk, logger)
    );

    const persistor = persistStore(store)
    return { persistor, store };
    
}