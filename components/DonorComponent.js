import React, { Component } from 'react';
import { View, Text, Image, ScrollView } from 'react-native'
import { Card, ListItem, Button, Icon, FlatList } from 'react-native-elements'
import * as Animatable from 'react-native-animatable';



class Donor extends Component {

    static navigationOptions = {
        title: 'Donor',
    };

    render() {
        const donors = [
            {
                name: 'Rahul',
                description :"+1234567890          O+          Mumbai"
            },
            {
                name: 'Gudiya',
                description :"+1234567890          AB+         Mumbai"
            },
            {
                name: 'Thomas',
                description :"+1234567890          B +         Mumbai"
                
            }

        ]


        return (
            <ScrollView>
                <Animatable.View animation="fadeInDown" duration={2000} delay={1000}>
                    <Card containerStyle={{ padding: 0 }} >
                        {
                            donors.map((u, i) => {
                                return (
                                    <ListItem
                                        key={i}                                        
                                        title={u.name}
                                        subtitle={u.description}
                                    />
                                );
                            })
                        }
                    </Card>
                </Animatable.View>

                <Animatable.View animation="fadeInDown" duration={2000} delay={1000}>
                    <Button
                        icon={<Icon name='person' color='floralwhite' />}
                        buttonStyle={{ borderRadius: 0, marginLeft: 15, marginRight: 15, marginBottom: 0, marginTop: 15, backgroundColor: '#B22222' }}
                        title='Become a donor' />
                </Animatable.View>

            </ScrollView>
        );
    }
}
export default Donor;