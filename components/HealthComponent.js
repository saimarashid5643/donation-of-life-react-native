import React, { Component } from 'react';
import { Card, Button, Icon } from 'react-native-elements';
import { Text } from 'react-native';
import * as Animatable from 'react-native-animatable';



class Health extends Component {

    static navigationOptions = {
        title: 'Health',
    };
    
    render() {        
        return (
            <Animatable.View animation="fadeInDown" duration={2000} delay={1000}>
                <Card
                    title='Why Donate Blood?'>
                    <Text style={{ margin: 10 }}>-- The most precious gift that one can give to another person is the gift of life i.e. blood. It is the essence of life.</Text>
                    <Text style={{ margin: 10 }}>-- Your blood saves more than one life when it is separated into its components (red blood cells, plasma etc.).</Text>
                    <Text style={{ margin: 10 }}> -- Blood is needed regularly for patients with diseases such as thalassemia and hemophilia, and also for the treatment of injuries after an accident, major surgeries, anemia, etc.</Text>
                    <Text style={{ margin: 10 }}> -- It improves your health.</Text>                                  
                    
                </Card>
            </Animatable.View>
        );
    }
}
export default Health;