import React, { Component } from 'react';
import { View, Animated, Easing } from 'react-native';
import { Card } from 'react-native-elements';

function RenderItem() {

    return (
        <Card title="Save Lives" wrapperStyle={{ padding: 0 }}                               
        image={require('./images/bd.jpg')}
        featuredTitle="Donate Blood"
        featuredSubtitle="Be a hero!">            
        </Card>
    );
}

   


class Home extends Component {

    constructor(props) {
        super(props);
        this.animatedValue = new Animated.Value(0);
    }

    static navigationOptions = {
        title: 'Home',
    };

    componentDidMount() {
        this.animate()
    }

    animate() {
        this.animatedValue.setValue(0)
        Animated.timing(
            this.animatedValue,
            {
                toValue: 8,
                duration: 8000,
                easing: Easing.linear
            }
        ).start(() => this.animate())
    }

    render() {

        const xpos1 = this.animatedValue.interpolate({
            inputRange: [0, 1, 2, 3, 4],
            outputRange: [1200, 800, 400, 200, 0]
        })
       

        return (
            <View>
                <Animated.View style={{ width: '100%', transform: [{ translateX: xpos1 }] }}>
                    <RenderItem />
                </Animated.View>                
            </View>
        );
    }
};
export default Home;