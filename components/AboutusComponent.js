import React, { Component } from 'react';
import { ScrollView, FlatList } from 'react-native';
import { ListItem, Card } from 'react-native-elements';
import { Text } from 'react-native';
import { connect } from 'react-redux';
import { baseUrl } from '../shared/baseUrl';
import { Loading } from './LoadingComponent';
import * as Animatable from 'react-native-animatable';


// const mapStateToProps = state => {
//     return {
//         leaders: state.leaders
//     }
// }
function History() {
    return (
        <Card title="About Us" wrapperStyle={{ padding: 0 }}>

            <Text style={{ margin: 10 }}>
            In India, despite having a lot of blood units available at all times, there is still a very high number of deaths which occur due to the lack of information about blood availability. This is the problem that my project idea aims to tackle. The website in consideration will be a one stop shop for any sort of information required on nearby availability of blood units, similar blood-type donors and their medical history. This will ensure that in case of an emergency, no lives are lost due to lack of information instead of lack of resources.
            </Text>
            <Text style={{ margin: 10 }}>
            I did not come across a single website which could provide me with each and every bit of information I might require in case of an emergency. This website aims to provide all the facilites at one platform. It will ensure that the purpose for such system in place should be fulfilled properly. People should be able find all the information regarding the available donors or blood units at a single place. They should not have to undergo the struggle of going through different websites under already difficult circumstances.
            </Text>
        </Card>
    );
}

class Aboutus extends Component {

    static navigationOptions = {
        title: 'About Us'
    };



    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <Animatable.View animation="fadeInDown" duration={2000} delay={1000}>
                    <History />                   
                </Animatable.View>
            </ScrollView>
        );
    }
                  
}


export default Aboutus;