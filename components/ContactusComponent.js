import React, { Component } from 'react';
import { Card, Button, Icon } from 'react-native-elements';
import { Text } from 'react-native';
import * as Animatable from 'react-native-animatable';
import * as MailComposer from 'expo-mail-composer';


class Contactus extends Component {

    static navigationOptions = {
        title: 'Contact Us',
    };
    sendMail() {
        MailComposer.composeAsync({
            recipients: ['donationoflife@gmail.com'],
            subject: 'Enquiry',
            body: 'To whom it may concern:'
        });
    }
    render() {        
        return (
            <Animatable.View animation="fadeInDown" duration={2000} delay={1000}>
                <Card
                    title='Contact Information'>
                    <Text style={{ margin: 10 }}> 91, Type-4</Text>
                    <Text style={{ margin: 10 }}> Power Colony,Ropar</Text>
                    <Text style={{ margin: 10 }}> Punjab, India</Text>
                    <Text style={{ margin: 10 }}> Tel: +91123654789</Text>
                    <Text style={{ margin: 10 }}> Fax: +91123654789</Text>                    
                    <Button
                        title="Send Email"
                        buttonStyle={{backgroundColor: "#B22222"}}
                        icon={<Icon name='envelope-o' type='font-awesome' color='white' />}
                        onPress={this.sendMail}
                        />
                </Card>
            </Animatable.View>
        );
    }
}
export default Contactus;