import React, { Component } from 'react';
import Home from './HomeComponent';
import Aboutus from './AboutusComponent';
import Contactus from './ContactusComponent';
import Health from './HealthComponent';
import Donor from './DonorComponent';
import { createStackNavigator, createDrawerNavigator, DrawerItems, SafeAreaView } from 'react-navigation';
import { View, Platform, ScrollView, Image, StyleSheet, Text, ToastAndroid } from 'react-native';
import { Icon } from 'react-native-elements';
import Login from './LoginComponent';
import NetInfo from '@react-native-community/netinfo';



const HomeNavigator = createStackNavigator({
  Home: { screen: Home }
}, {
  navigationOptions: ({ navigation }) => ({
    headerStyle: {
      backgroundColor: "#B22222"
    },
    headerTitleStyle: {
      color: "#fff"
    },
    headerTintColor: "#fff",

    headerLeft: <Icon name="menu" size={24}
      color='white'
      onPress={() => navigation.toggleDrawer()} />
  })
});

const DonorNavigator = createStackNavigator({
  Donor: { screen: Donor }
}, {
  navigationOptions: ({ navigation }) => ({
    headerStyle: {
      backgroundColor: "#B22222"
    },
    headerTitleStyle: {
      color: "#fff"
    },
    headerTintColor: "#fff",

    headerLeft: <Icon name="menu" size={24}
      color='white'
      onPress={() => navigation.toggleDrawer()} />
  })
});

const HealthNavigator = createStackNavigator({
  Health: { screen: Health }
}, {
  navigationOptions: ({ navigation }) => ({
    headerStyle: {
      backgroundColor: "#B22222"
    },
    headerTitleStyle: {
      color: "#fff"
    },
    headerTintColor: "#fff",

    headerLeft: <Icon name="menu" size={24}
      color='white'
      onPress={() => navigation.toggleDrawer()} />
  })
});

const AboutusNavigator = createStackNavigator({
  Aboutus: { screen: Aboutus }
}, {
  navigationOptions: ({ navigation }) => ({
    headerStyle: {
      backgroundColor: "#B22222"
    },
    headerTitleStyle: {
      color: "#fff"
    },
    headerTintColor: "#fff",
    headerLeft: <Icon name="menu" size={24}
      color='white'
      onPress={() => navigation.toggleDrawer()} />
  })
});

const ContactusNavigator = createStackNavigator({
  HomeContactus: { screen: Contactus }
}, {
  navigationOptions: ({ navigation }) => ({
    headerStyle: {
      backgroundColor: "#B22222"
    },
    headerTitleStyle: {
      color: "#fff"
    },
    headerTintColor: "#fff",
    headerLeft: <Icon name="menu" size={24}
      color='white'
      onPress={() => navigation.toggleDrawer()} />
  })
});

const LoginNavigator = createStackNavigator({
  Login: Login
}, {
  navigationOptions: ({ navigation }) => ({
    headerStyle: {
      backgroundColor: "#B22222"
    },
    headerTitleStyle: {
      color: "#fff"
    },
    title: 'Login',
    headerTintColor: "#fff",
    headerLeft: <Icon name="menu" size={24}
      iconStyle={{ color: 'white' }}
      onPress={() => navigation.toggleDrawer()} />
  })
});

const CustomDrawerContentComponent = (props) => (
  <ScrollView>
    <SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
      <View style={styles.drawerHeader}>
        <View style={{ flex: 1 }}>
          <Image source={require('./images/dol.png')} style={styles.drawerImage} />
        </View>
        <View style={{ flex: 2 }}>
          <Text style={styles.drawerHeaderText}>Donation Of Life</Text>
        </View>
      </View>
      <DrawerItems {...props} />
    </SafeAreaView>
  </ScrollView>
);

const MainNavigator = createDrawerNavigator({
  Login:
  {
    screen: LoginNavigator,
    navigationOptions: {
      title: 'Login',
      drawerLabel: 'Login',
      drawerIcon: ({ tintColor, focused }) => (
        <Icon
          name='sign-in'
          type='font-awesome'
          size={24}
          iconStyle={{ color: tintColor }}
        />
      ),
    }
  },
  Home:
  {
    screen: HomeNavigator,
    navigationOptions: {
      title: 'Home',
      drawerLabel: 'Home',
      drawerIcon: ({ tintColor, focused }) => (
        <Icon
          name='home'
          type='font-awesome'
          size={24}
          color={tintColor}
        />
      ),
    }
  },
  Aboutus:
  {
    screen: AboutusNavigator,
    navigationOptions: {
      title: 'About Us',
      drawerLabel: 'About Us',
      drawerIcon: ({ tintColor, focused }) => (
        <Icon
          name='info-circle'
          type='font-awesome'
          size={24}
          color={tintColor}
        />
      ),
    }
  },
  
  
  Donor:
  {
    screen: DonorNavigator,
    navigationOptions: {
      title: 'Donor',
      drawerLabel: 'Donor',
      drawerIcon: ({ tintColor, focused }) => (
        <Icon
          name='list'
          type='font-awesome'
          size={24}
          color={tintColor}
        />
      ),
    }
  },
  Contactus:
  {
    screen: ContactusNavigator,
    navigationOptions: {
      title: 'Contact Us',
      drawerLabel: 'Contact Us',
      drawerIcon: ({ tintColor, focused }) => (
        <Icon
          name='address-card'
          type='font-awesome'
          size={22}
          color={tintColor}
        />
      )
    }
  },
  Health:
  {
    screen: HealthNavigator,
    navigationOptions: {
      title: 'Health',
      drawerLabel: 'Health',
      drawerIcon: ({ tintColor, focused }) => (
        <Icon
          name='heartbeat'
          type='font-awesome'
          size={24}
          iconStyle={{ color: tintColor }}
        />
      ),
    }
  }
 
},
//#dcdcdc
//#FFB6C1
  {
    initialRouteName: 'Home',
    drawerBackgroundColor: '#dcdcdc',
    contentComponent: CustomDrawerContentComponent
  });

class Main extends Component {

  componentDidMount() {
    this.props.fetchDishes();
    this.props.fetchComments();
    this.props.fetchPromos();
    this.props.fetchLeaders();
    NetInfo.fetch().then((connectionInfo) => {
      ToastAndroid.show('Initial Network Connectivity Type: '
          + connectionInfo.type, ToastAndroid.LONG)
  });
  
  NetInfo.addEventListener(connectionChange => this.handleConnectivityChange(connectionChange))
}

componentWillUnmount() {
  NetInfo.removeEventListener(connectionChange => this.handleConnectivityChange(connectionChange))
}

handleConnectivityChange = (connectionInfo) => {
switch (connectionInfo.type) {
    case 'none': 
        ToastAndroid.show ('You are now offline', ToastAndroid.LONG);
        break;
    case 'wifi':
        ToastAndroid.show ('You are now on WiFi', ToastAndroid.LONG);
        break;
    case 'cellular':
        ToastAndroid.show ('You are now on Cellular', ToastAndroid.LONG);
        break;
    case 'unknown' :
        ToastAndroid.show ('You are now have an Unknown connection', ToastAndroid.LONG);
        break;
    default: 
}
}

  render() {
    return (
      <View style={{ flex: 1, paddingTop: Platform.OS === 'ios' ? 0 : Expo.Constants.statusBarHeight }}>
        <MainNavigator />
      </View>
    );
  }
}
export default Main;



const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  drawerHeader: {
    backgroundColor: '#B22222',
    height: 140,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row'
  },
  drawerHeaderText: {
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold'
  },
  drawerImage: {
    margin: 10,
    width: 80,
    height: 60
  }
});